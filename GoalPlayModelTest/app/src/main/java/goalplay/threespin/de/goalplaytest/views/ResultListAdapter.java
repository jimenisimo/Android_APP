package goalplay.threespin.de.goalplaytest.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import goalplay.threespin.de.goalplaytest.R;

public class ResultListAdapter extends ArrayAdapter<Integer> {

    private final LayoutInflater inflater;
    private List<Integer> testData = new ArrayList<>();
    private String[] itemTexts;
    private Float AccuracyPercentage;

    public ResultListAdapter(Context context){
        super(context, R.layout.listview_item);

        itemTexts = context.getResources().getStringArray(R.array.result_array);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(List<Integer> data, List<Integer> testdata){

        testData = testdata;

        if (data != null){
            for (Integer intvalue : data){
                add(intvalue);
            }
        }
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent){

        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.listview_item, parent, false);
        } else {
            view = convertView;
        }

        Integer intValue = getItem(position);
        ((TextView) view.findViewById(R.id.item_text)).setText(""+intValue+" | "+itemTexts[intValue-1]);


        if (testData!=null){
            view.findViewById(R.id.item_text_test).setVisibility(View.VISIBLE);

            ((TextView)view.findViewById(R.id.item_text_test)).setText(""+testData.get(position)+" | "+itemTexts[testData.get(position)-1]);

            if (testData.get(position) == intValue){
                view.setBackgroundResource(R.color.colorValid);
            } else {
                view.setBackgroundResource(R.color.colorInvalid);
            }

        } else {
            view.findViewById(R.id.item_text_test).setVisibility(View.GONE);
            view.setBackgroundResource(R.color.colorWhite);
        }

        return view;
    }
}
