package goalplay.threespin.de.goalplaytest;


import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.RelativeLayout;

import org.tensorflow.lite.Interpreter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Vector;

import goalplay.threespin.de.goalplaytest.views.ResultListAdapter;

public class MainActivity extends AppCompatActivity  {

    private static int PERMISSION_REQUEST_EXTERNAL_STORAGE = 101;

    private static final String MODEL_FILE = "estimator_model.tflite";

    private Interpreter tfLiteInterpreter;

    private ListView listView;
    private RelativeLayout loadingOverlay;

    private TextView accuracyText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listview);
        accuracyText = findViewById(R.id.textView);
        loadingOverlay = findViewById(R.id.loadingOverlay);

        try {
            tfLiteInterpreter = new Interpreter(loadModelFile());
        } catch (IOException e){
            e.printStackTrace();
        }

        /*
        try {
            tfLiteInterpreter = new Interpreter(loadModelFromFile(new File(getDataFolder(),"model.lite")));
        }catch (IOException e){
            e.printStackTrace();
        }*/

        if (tfLiteInterpreter == null){
            Log.d("cj","error loading model file!");
        }

        refreshData();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void refreshData(){

        loadingOverlay.setVisibility(View.VISIBLE);

        File dataFolder = getDataFolder();

        File inputData = new File(dataFolder,"input2.txt");
        if (!inputData.exists()){
            Log.d("cj","input data file doesn't exist!");
        }

        File testData = new File(dataFolder,"test.txt");
        if (!inputData.exists()){
            Log.d("cj","test data file doesn't exist!");
        }


        ArrayList<Vector<Float>> input = readInput(inputData);
        ArrayList<Integer> testOutput = readTestData(testData);

        //run tensorflow
        ArrayList<Integer> output = runTensorFlow(input);

        compareArrays(testOutput,output);
        //change to output
        writeOutputToFile(output);


        ResultListAdapter adapter = new ResultListAdapter(this);
        adapter.setData(output,testOutput);

        listView.setAdapter(adapter);
        listView.setClickable(true);

        loadingOverlay.setVisibility(View.INVISIBLE);
    }

    public void compareArrays( ArrayList<Integer> array1,  ArrayList<Integer> array2) {
        Integer sameValue = 0;

        if (array1 != null && array2 != null){
            if (array1.size() != array2.size())
                sameValue = -1;
            else
                for (int i = 0; i < array2.size(); i++) {
                    int output =array2.get(i);
                    int test =array1.get(i);
                    if (array2.get(i).equals(array1.get(i))) {
                        sameValue++;
                    }
                }
        }else{
            sameValue = -1;
        }
        System.out.println("Accuracy: "+ ((sameValue*1.0/ array2.size())*100)+ "%" );
        Log.d("cj","Aciertos: " + sameValue);
        Log.d("cj","Accuracy: "+ ((sameValue*1.0/ array2.size())*100)+ "%");

        accuracyText.setText("Accuracy: "+ String.format("%.2f",((sameValue*1.0/ array2.size())*100))+ "%");


    }

    private ArrayList<Integer> runTensorFlow(ArrayList<Vector<Float>> input) {

        ArrayList<Integer> output = new ArrayList<>();

        float[][] wrappedValues = new float[input.size()][10];
        for (int j=0; j < input.size(); j++){


            float[] values = new float[10];
            for (int i=0; i < 10; i++){
                values[i] = input.get(j).get(i);
            }

            wrappedValues[j] = values;
        }

        float[][] outputValue = new float[input.size()][5];

        tfLiteInterpreter.run(wrappedValues,outputValue);

        for (int k=0; k < input.size(); k++){

            float[] outputs = outputValue[k];
            int max = 0;
            for (int m=0; m < outputs.length; m++){

                if (outputs[m] > outputs[max]){
                    max = m;
                }
            }

            max++;
            output.add(max);
        }

        return output;
    }

    private File getDataFolder(){

        File dataFolder = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS),"goalplaydata");
        if (!dataFolder.exists()){
            if (!dataFolder.mkdirs()){
                Log.d("cj","error creating folder");
            }
        }

        return dataFolder;
    }

    private ArrayList<Vector<Float>> readInput(File inputfile){

        ArrayList<String> lines = new ArrayList<>();
        BufferedReader reader;
        try {
            FileInputStream inputStream = new FileInputStream(inputfile);
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.readLine();
            while (line != null){
                lines.add(line);
                line = reader.readLine();
            }

        }catch (IOException e){
            e.printStackTrace();
        }

        ArrayList<Vector<Float>> output = new ArrayList<>();
        for (String line : lines){
            if (line.contains("spectral")){
                //ignore first line
                continue;
            }
            String[] values = line.split(",");
            Vector<Float> vector = new Vector<>();
            for (int i=0; i < values.length; i++){
                vector.add(Float.parseFloat(values[i]));
            }
            output.add(vector);
        }

        return output;
    }

    private ArrayList<Integer> readTestData(File testfile){

        ArrayList<Integer> output = new ArrayList<>();
        BufferedReader reader;
        try {
            FileInputStream inputStream = new FileInputStream(testfile);
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.readLine();
            while (line != null){
                int index = Integer.parseInt(line);
                output.add(index);
                line = reader.readLine();
            }

        }catch (IOException e){
            e.printStackTrace();
        }

        return output;
    }

    private void writeOutputToFile(ArrayList<Integer> output){

        File outputFile = new File(getDataFolder(),"output.txt");
        FileOutputStream outputStream;

        try{
            outputStream = new FileOutputStream(outputFile);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            for (Integer s : output){
                writer.write(""+s);
                writer.newLine();
            }
            writer.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private MappedByteBuffer loadModelFile() throws IOException {

        AssetFileDescriptor fileDescriptor = getAssets().openFd(MODEL_FILE);

        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());

        FileChannel fileChannel = inputStream.getChannel();

        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();

        return fileChannel.map(FileChannel.MapMode.READ_ONLY,startOffset,declaredLength);
    }

    private MappedByteBuffer loadModelFromFile(File modelFile) throws IOException{

        RandomAccessFile randomAccessFile = new RandomAccessFile(modelFile,"r");
        MappedByteBuffer mappedByteBuffer = randomAccessFile.getChannel().map(FileChannel.MapMode.READ_ONLY,0,randomAccessFile.length());

        byte[] data = new byte[100];

        while (mappedByteBuffer.hasRemaining()){
            int remaining = data.length;
            if (mappedByteBuffer.remaining() < remaining){
                remaining = mappedByteBuffer.remaining();
            }

            mappedByteBuffer.get(data,0,remaining);
        }


        return mappedByteBuffer;
    }

    public void refresh(View v){

        if (v.getId() == R.id.refreshBtn){

            refreshData();
        }
    }

    private void permissionExternalStorageHasChanged(boolean hasPermission){

        if (hasPermission){
            refreshData();
        } else {
            //alert
        }
    }

    @Override
    public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
                                             @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == PERMISSION_REQUEST_EXTERNAL_STORAGE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //permission to get images from gallery granted
                permissionExternalStorageHasChanged(true);
            }
        }
    }
}
